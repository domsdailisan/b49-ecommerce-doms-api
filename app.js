const express = require("express")
const app = express()
const PORT = process.env.PORT || 4000
const cors = require("cors")
const mongoose = require("mongoose")

//MongoDB Connection
// mongoose.connect("mongodb+srv:mongodb+srv://Doms1234:Doms1234@b49-ecommerce-82cxa.mongodb.net/test?retryWrites=true&w=majority"
// mongoose.connect("mongodb://localhost/b49-ecommerce"
mongoose.connect("mongodb+srv://Doms1234:Doms1234@b49-ecommerce-82cxa.mongodb.net/test?retryWrites=true&w=majority", {
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
})

const db = mongoose.connection
db.once("open", () => console.log("We are connected to MongoDB")) //check if we are connected with our db

app.use(cors()) //to connect with frontend
app.use(express.json()) //to use JSON on server?
app.use(express.static('public')) //to access on browser the files. http://localhost:4000/products/image

//Define routes/resources\
app.use("/users", require("./routes/users"))
app.use("/products", require("./routes/products"))
app.use("/categories", require("./routes/categories"))
app.use("/transactions", require("./routes/transactions"))

app.listen(PORT, () => console.log(`Server is running on port ${PORT}`)) //check if server is up
